from django.db import models

# Create your models here.

class Destination(models.Model):


    name = models.CharField(max_length=30)
    img = models.ImageField(upload_to='pics')
    desc = models.TextField()
    price = models.IntegerField()
# class PersonManager(BaseUserManager):
#     def create_user(self, email,date_of_birth, username,password=None,):
#         if not email:
#             msg = 'Users must have an email address'
#             raise ValueError(msg)
#         if not username:
#             msg = 'This username is not valid'
#         raise ValueError(msg)
#         if not date_of_birth:
#             msg = 'Please Verify Your DOB'
#             raise ValueError(msg)
#         user = self.model(
# email=PersonManager.normalize_email(email),username=username,date_of_birth=date_of_birth)
#         user.set_password(password)
#         user.save(using=self._db)
#         return user
#     def create_superuser(self,email,username,password,date_of_birth):
#         user = self.create_user(email,password=password,username=username,date_of_birth=date_of_birth)
#         user.is_admin = True
#         user.is_staff = True
#         user.is_superuser = True
#         user.save(using=self._db)
#         return user

# class Person(AbstractBaseUser, PermissionsMixin):
#     email = models.EmailField(verbose_name='email address',max_length=255,unique=True,db_index=True,)
#     username = models.CharField(max_length=255, unique=True)
#     date_of_birth = models.DateField()
#     USERNAME_FIELD = 'email'
#     REQUIRED_FIELDS = ['username', 'date_of_birth',]
#     is_active = models.BooleanField(default=True)
#     is_admin = models.BooleanField(default=False)
#     is_staff = models.BooleanField(default=False)
#     objects = PersonManager()
#     def get_full_name(self):
#         return self.email

#     def get_short_name(self):
#         return self.email

#     def __unicode__(self):
#         return self.email
