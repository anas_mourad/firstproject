from django.contrib.auth import get_user_model
from django.urls import include
from django.urls.conf import path
from django.urls import path
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from . import views
# # Serializers define the API representation.
# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         User=get_user_model()
#         model = User
#         fields = ['url', 'username', 'email', 'is_staff']


# # ViewSets define the view behavior.
# class UserViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer


# # Routers provide a way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r'users', UserViewSet)
# urlpatterns = [
#     path('', include(router.urls)),
#     path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
# ]
urlpatterns = [
    path('',views.home,name='home'),
    path('add',views.add,name='add'),
     
]