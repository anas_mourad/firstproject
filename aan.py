# Python3 program to find the count of
# smaller elements on right side of
# each element in an Array
# using Merge sort
N = 100001
ans = [0] * N

# Utility function that merge the array
# and count smaller element on right side
def merge(a, start, mid, end):

	f = [0] * (mid - start + 1)
	s = [0] * (end - mid)
					
	n = mid - start + 1
	m = end - mid
	
	for i in range(start, mid + 1):
		f[i - start] = a[i]
	for i in range ( mid + 1, end + 1):
		s[i - mid - 1] = a[i]
		
	i = 0
	j = 0
	k = start
	cnt = 0

	# Loop to store the count of smaller
	# Elements on right side when both
	# Array have some elements
	while (i < n and j < m):
		if (f[i][1] <= s[j][1]):
			ans[f[i][0]] += cnt
			a[k] = f[i]
			k += 1
			i += 1
			
		else:
			cnt += 1
			a[k] = s[j]
			k += 1
			j += 1
    print('hi')
	
	# Loop to store the count of smaller
	# elements in right side when only
	# left array have some element
	while (i < n):
		ans[f[i][0]] += cnt
		a[k] = f[i];
		k += 1
		i += 1
	
	# Loop to store the count of smaller
	# elements in right side when only
	# right array have some element
	while (j < m):
		a[k] = s[j]
		k += 1
		j += 1
	
# Function to invoke merge sort.
def mergesort(item, low, high):

	if (low >= high):
		return
		
	mid = (low + high) // 2
	mergesort(item, low, mid)
	mergesort(item, mid + 1, high)
	merge(item, low, mid, high)

# Utility function that prints
# out an array on a line
def print_(arr, n):

	for i in range(n):
		print(arr[i], end = " ")

# Driver code.
if __name__ == "__main__":

	arr = [ 10, 9, 5, 2, 7,
			6, 11, 0, 2 ]
					
	n = len(arr)
	a = [[0 for x in range(2)]
			for y in range(n)]
	
	for i in range(n):
		a[i][1] = arr[i]
		a[i][0] = i
	
	mergesort(a, 0, n - 1)
	print_(ans, n)
	
# This code is contributed by chitranayal
